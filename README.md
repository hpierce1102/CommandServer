Architecture
=============

CommandEmitter
--------------

CommandEmitters are the smallest unit of work that warrants thinking about. CommandEmitters
are responsible for running a single instance of a given command. After their execution
they terminate themselves. At all stages of the command's lifecycle, it broadcasts its status to
the client.

It will broadcast it's status at various stages of its lifecycle such as: initialization,
data emitting, and termination.


CommandEmitterCollection 
------------------------

Contains and manages all commands that are being tracked. The browser can send commands to 
the server that can _attach_ commands to this index to keep track of them and _detach_ them
to stop listening to them.

It's important to note that a command can be tracked in this index before it has started executing
and after it has been terminated.


Messages
========

The examples on this page will demonstrate how to interactions with respect to the browser.
That is to say, all these examples are examples of JavaScript that should go on the client.

Client to Server:
=================

These are the messages that get sent from the browser to the server.

system.attach:
------------

Creates a new command and tracks it in the CommandEmitterCollection.

```
socket.emit('system.attach', {
    command : 'ls',
    arguments : ['l', 'a']
});
```

is equivalent to

```
$ ls -la
```

system.detach
------------

Terminates and removes a command from the CommandEmitterCollection
```
socket.emit('system.detach', {
    token: 'RandomString'
});
```

system.detachAll
----------------

Terminates and removes everything.
```
socket.emit('system.detachAll', {});
```

Server to Client
================


system.attached
---------------

A new command has been registered in the CommandEmitterCollection. It will be initiated
immediately after.

The token returned uniquely identifies the command that's running on the server.

```
socket.on('system.attached', function(msg){
    console.log(msg);
    // {
    //    token : 'RandomBytes'
    // }
});
```

system.detached
---------------

A command has been removed from the CommandEmitterCollection.

```
socket.on('system.attached', function(msg){
    console.log(msg);
    // {
    //    token : 'RandomBytes'
    // }
});
```

system.error
------------

An error occurred that is not specific to a certain command.

```
socket.on('system.attached', function(msg){
    console.log(msg);
    // {
    //    message : 'string'
    // }
});
```

command.init
------------

Definitive evidence that the server has successfully executed the command.
Output will be sent soon.

```
socket.on('command.init', function(msg){
    console.log(msg);
    // {
    //    token : 'RandomBytes'
    // }
});
```

command.output
--------------

Yields a single line of output (STDOUT) from the command identified by the token.

```
socket.on('command.output', function(msg){
    console.log(msg);
    // {
    //    token : 'RandomBytes',
    //    output : 'a line of output from the command'
    // }
});
```

command.error
-------------

Yields a single line of output (STRERR) from the command identified by the token.


```
socket.on('command.error', function(msg){
    console.log(msg);
    // {
    //    token : 'RandomBytes',
    //    output : 'a line of error from the command'
    // }
});
```

command.terminated
------------------

The command has ended and will no longer provide output.

```
socket.on('command.terminated', function(msg){
    console.log(msg);
    // {
    //    token : 'RandomBytes',
    // }
});
```
