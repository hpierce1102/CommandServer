let app = new require('express')();
let http = new require('http').Server(app);
let ioFactory = new require('./src/Infrastructure/SocketIoFactory.js');
const CommandEmitterCollection = require('./src/CommandEmitterCollection.js');
const CommandWhitelistMiddleware = require('./src/CommandWhitelistMiddleware.js');
const CommandEmitterException = require('./src/CommandEmitterException');
const Middleware = require('./src/Infrastructure/middleware');
const fs = require('fs');

const util = require('util');
const readFile = util.promisify(fs.readFile);

class CommandServer {
    static init(configObject) {
        if( !(configObject instanceof CommandServerConfig)) {
            throw 'CommandServer.init expects a CommandServerConfig object. Recieved ' + typeof configObject;
        }

        const SystemAttachMiddleware = new Middleware();
        SystemAttachMiddleware.register([
            new CommandWhitelistMiddleware(configObject)
        ]);

        let io = ioFactory(http, configObject);

        io.on('connection', function(socket){
            let commandCollection = new CommandEmitterCollection(io);

            socket.on('system.attach', (command) => {
                command = SystemAttachMiddleware.execute(command);

                if(command instanceof CommandEmitterException) {
                    io.emit('system.error', {
                        message : command.message
                    });
                } else {
                    commandCollection.attach(command);
                }

            });

            socket.on('system.detach', (msg) => {
                commandCollection.detach(msg.token);
            });

            socket.on('disconnect', function(signal){
                commandCollection.detachAll();
            });
        });

        let port = configObject.port || 3001;

        http.listen(port, function(){
            console.log('CommandServer listening on *:' + port);
        })
    }
}

class CommandServerConfig {
    static async createFromFile(filePath) {
        let file = await readFile(filePath);
        let configOptions = JSON.parse(file);

        configOptions.configFile = filePath;
        return CommandServerConfig.create(configOptions);
    }

    static create(options) {


        let config = new CommandServerConfig();
        config.port = options.port || 3001;
        config.whitelist = options.whitelist || [];
        config.configFile = options.configFile || null;
        config.logging = options.logging || false;

        return config;
    }

    getWhitelist() {
        let whitelist;

        if(typeof this.configFile === 'string' ) {
            let file = fs.readFileSync(this.configFile);
            let config = JSON.parse(file);

            whitelist = config.whitelist;
        }

        return whitelist || this.whitelist;
    }
}

module.exports = {
    CommandServer,
    CommandServerConfig
};