A CommandServer instance relies on a CommandServerConfig object to provide numerous options.

```
let config = CommandServerConfig.create({
    "whitelist" : [ 'ping' ],
    "port" : 3001,
    "logging" : true
});
CommandServer.init(config);
```

Options
========

Whitelist
---------

Array of commands that the server will allow. If a command is not in this list, the command server will not attempt to execute it.

Port
----

The port that the command server listens to. This is not nessesarily the same as the port for an application using command server.
Because most Express apps will use port 3000, config will default to port 3001.

logging
-------

Boolean. If true, any actions will be logged to the console. This is useful for debugging purposes.