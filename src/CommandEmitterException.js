class CommandEmitterException {
    constructor(message) {
        this.message = message;
    }
}

module.exports = CommandEmitterException;