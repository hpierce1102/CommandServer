const child_process = require('child_process');

const statusNotInitialized = 1;
const statusInitialized = 2;
const statusTerminated = 3;

class CommandEmitter {

    constructor(commandOptions, io) {
        this.commandOptions = commandOptions;
        this.io = io;
        this.process = null;
        this.token = null;
        this.status = statusNotInitialized;
    }

    setToken(token) {
        this.token = token;
    }

    start() {
        this.status = statusInitialized;
        this.process = child_process.spawn(this.commandOptions.command, this.commandOptions.arguments);
        this.process.stdout.on('data', (data) => this.io.emit('command.output', {
            token : this.token,
            output : data.toString()
        }));
        this.process.on('error', (err) => this.io.emit('command.error', {
            token : this.token,
            output : err
        }));
        this.process.on('exit', (err) => {
            this.kill();
        });

        this.io.emit('command.init', {
            token : this.token
        })
    }

    kill() {
        if(!this.isTerminated()) {
            this.process.kill();
            this.status = statusTerminated;

            this.io.emit('command.terminated', {
                token : this.token
            })
        }

    }

    isTerminated() {
        return this.status >= statusTerminated;
    }
}

module.exports = CommandEmitter;