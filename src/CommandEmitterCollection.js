const crypto = require('crypto');
const CommandEmitter = require('./CommandEmitter');

class CommandEmitterCollection {
    constructor(io) {
        this.commandEmitters = [];
        this.io = io;
    }

    attach(commandEmitterOptions, io) {
        let commandEmitter = new CommandEmitter(commandEmitterOptions, this.io);
        let token = generateToken(this);

        commandEmitter.setToken(token);

        this.commandEmitters.push({
            token : token,
            commandEmitter : commandEmitter
        });

        this.io.emit('system.attached', {
            token : token,
        });

        commandEmitter.start();

        return token;
    }

    detach(token) {
        let existingEmitterCount = this.commandEmitters.length;

        this.commandEmitters = this.commandEmitters.filter( emitterData => {
            let shouldRemoveEmitter  = emitterData.token === token;

            if(shouldRemoveEmitter) {
                emitterData.commandEmitter.kill();
            }

            return !shouldRemoveEmitter;
        });

        let newEmitterCount = this.commandEmitters.length;

        if(existingEmitterCount === newEmitterCount) {
            this.io.emit('system.error', {
                message : "Could not detach emitter; no emitter with that token.",
            });
        } else {
            this.io.emit('system.detached', {
                token : token,
            });
        }

        return true;
    }

    detachAll() {
        this.commandEmitters.forEach( emitter => {
            this.detach(emitter.token);
        });

        return true;
    }
}

function generateToken(collection){
    let token = crypto.randomBytes(10).toString('hex');

    let isTokenAlreadyInUse = collection.commandEmitters.filter(emitter => {
        return emitter.token === token;
    }).length > 0;

    if(isTokenAlreadyInUse) {
        return generateToken();
    } else {
        return token;
    }
}

module.exports = CommandEmitterCollection;