const CommandEmitterException =  require('../CommandEmitterException.js');
const MiddlewareInterface = require('./middlewareInterface.js');

class Middleware {
    constructor() {
        this.middlewareObjects = [];
    }

    register(middlewareObjects) {
        let invalidMiddleware = middlewareObjects.filter( (middleware) => {
            return !(middleware instanceof MiddlewareInterface);
        });

        if (invalidMiddleware.length > 0) {
            console.warn('The following middleware do not extend the MiddlewareInterface: ' + invalidMiddleware.join(', '));
        }

        this.middlewareObjects = middlewareObjects;
    }

    execute (initialValue) {
        let finalValue = this.middlewareObjects.reduce(function(value, middleware){
            if (value instanceof CommandEmitterException) {
                return value;
            } else {
                return middleware.run(value);
            }
        }, initialValue);

        return finalValue;
    }
}

module.exports = Middleware;