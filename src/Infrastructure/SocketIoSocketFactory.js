module.exports = function (socket, configObject) {
    let socketExtension = Object.create(socket);
    socketExtension.on = function(eventName, callback) {
        let socketNewCallback = function(data) {
            if(configObject.logging === true) {
                console.log({
                    "socket-event-received": eventName,
                    "data": data
                });
            }
            return callback(data);
        };

        return socket.on(eventName, socketNewCallback);
    };

    return socketExtension;
};