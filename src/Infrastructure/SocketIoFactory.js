/**
 * The purpose of this file is to extend the default socket.io SERVER to include logging of
 * incoming requests and outgoing messages. It also calls out to the SOCKET factory so that the socket factory
 * file can extend the socket to allow for logging on specific connections.
 */

let ioFactory = new require('socket.io');
const SocketFactory = require('./SocketIoSocketFactory.js');

module.exports = function(http, configObject) {
    let io = new ioFactory(http);

    let ioExtension = Object.create(io);

    ioExtension.on = function(event, callback) {
        let newCallback = function(socket) {

            if(configObject.logging === true) {
                console.log({
                    "server-event-received" : event
                });
            }

            // This is where we generate the extended socket object that we expose to the typical callback.
            let customSocket = SocketFactory(socket, configObject);

            // The new logging-aware-socket is provided to the function.
            callback(customSocket);
        };

        return io.on(event, newCallback)
    };

    ioExtension.emit = function(eventName, args, ack) {
        if(configObject.logging === true) {
            console.log({
                "event-emitted": eventName,
                "data": args
            });
        }
        return io.emit(eventName, args, ack)
    };

    return ioExtension;
};