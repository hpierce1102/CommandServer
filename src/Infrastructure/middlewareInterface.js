class MiddlewareInterface {
    run(value) {
        throw typeof this + ' does not implement the run method.';
    }
}

module.exports = MiddlewareInterface;