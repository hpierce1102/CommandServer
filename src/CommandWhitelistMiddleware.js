const MiddlewareInterface = require('./Infrastructure/middlewareInterface.js');
const CommandEmitterException = require('./CommandEmitterException');

const fs = require('fs');

const defaultConfigFile = "/../CommandWhitelistConfig.json";
class CommandWhitelistMiddleware extends MiddlewareInterface {

    constructor(config) {
        super();
        this.config = config;
    }

    run(command) {
        let allowedCommands = this.config.getWhitelist();

        if(allowedCommands.indexOf(command.command) === -1) {
            return new CommandEmitterException('This command is not on the whitelist');
        }

        return command;
    }
}

module.exports = CommandWhitelistMiddleware;